<?php


namespace App\Services;


use App\User;

class UserService
{
    /**
     * @var TokenGenerator
     */
    private TokenGenerator $tokenGenerator;

    /**
     * UserService constructor.
     * @param TokenGenerator $tokenGenerator
     */
    public function __construct(TokenGenerator $tokenGenerator)
    {
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param string $name
     * @return User
     */
    public function create(string $name): User
    {
        return User::forceCreate([
            'name' => $name,
            'api_token' => $this->tokenGenerator->getHashedToken()
        ]);
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->tokenGenerator->getToken();
    }
}
