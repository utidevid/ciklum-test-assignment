<?php


namespace App\Services;

use Str;

class TokenGenerator
{
    /**
     * @var string
     */
    private string $apiToken;

    public function __construct()
    {
        $this->apiToken = Str::random(80);
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->apiToken;
    }

    /**
     * @return string
     */
    public function getHashedToken(): string
    {
        return hash('sha256', $this->apiToken);
    }
}
