<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Request;

class UsersController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * RegisterController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('guest');
        $this->userService = $userService;
    }

    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        Request::validate([
            'name' => ['required', 'max:100', 'string'],
        ]);


        $this->userService->create(Request::json('name'));

        return \Response::json(['token' => $this->userService->getToken()], 201);
    }
}
