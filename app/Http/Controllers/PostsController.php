<?php

namespace App\Http\Controllers;

use App\Managers\PostManager;
use App\Policies\PostPolicy;
use App\Post;
use App\User;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Post $post)
    {
        return \Response::json([
            'title' => $post->title,
            'content' => $post->content,
            'description' => $post->description,
            'created_at' => $post->created_at
        ]);
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return Auth::user()->posts()->get();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $validationAttrs = \Request::validate([
            'title' => ['required', 'min:3', 'max:255', 'string'],
            'content' => ['required', 'min:3', 'max:255', 'string'],
            'description' => ['required', 'min:3', 'max:255', 'string'],
        ]);

        /** @var User $user */
        $user = Auth::user();
        $post = $user->posts()->create($validationAttrs);

        return \Response::json([
            'id' => $post->id,
            'title' => $post->title,
            'content' => $post->content,
            'description' => $post->description
        ], 201);
    }

    /**
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Post $post)
    {
        $validationAttrs = Request::validate([
            'title' => ['min:3', 'max:255', 'string'],
            'content' => ['min:3', 'max:255', 'string'],
            'description' => ['min:3', 'max:255', 'string'],
        ]);

        if ($post->update($validationAttrs)) {
            return \Response::json([
                'title' => $post->title,
                'content' => $post->content,
                'description' => $post->description
            ], 200);
        }

        return \Response::json(['error' => ['msg' => 'Something went wrong!']], 500);
    }

    /**
     * @param PostManager $postManager
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function delete(PostManager $postManager)
    {
        $ids = Request::get('ids');

        if (!$ids) {
            throw new BadRequestHttpException();
        }

        if (is_numeric($ids)) {
            $post = Post::find($ids);
            if (!$post) {
                throw new ModelNotFoundException();
            }

            $this->authorize('delete', $post);
            $post->delete();
        } else {
            $ids = explode(',', $ids, PostPolicy::MAX_POSTS_TO_DELETE);
            $postManager->deleteMultiple($ids);
        }

        return \Response::json(null, 204);
    }
}
