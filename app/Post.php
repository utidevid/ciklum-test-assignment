<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 * @package App
 * @mixin Eloquent
 */
class Post extends Model
{
    public $timestamps = false;

    protected $fillable = ['title', 'content', 'description'];

    //private $guarded = [];

    /**
     * Path to the current post
     *
     * @return string
     */
    public function path()
    {
        return route('posts.get', $this);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
