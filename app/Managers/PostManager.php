<?php


namespace App\Managers;

use Illuminate\Database\Query\Builder;

class PostManager
{
    /**
     * @param array $ids
     * @return int how many records were affected
     */
    public function deleteMultiple(array $ids): int
    {
        if ($ids) {
            // Filter out everything except numeric values
            $ids = array_filter($ids, fn($val) => is_numeric($val));

            $deletionQuery = \DB::table('posts')->where(function (Builder $query) use ($ids) {
                $query->whereIn('id', $ids)
                    ->where('user_id', '=', auth()->id());
            })->delete();

            return $deletionQuery;
        }

        return 0;
    }
}
