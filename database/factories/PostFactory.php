<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

$factory->define(\App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'content' => $faker->text(100),
        'description' => $faker->text(100),
        'user_id' => factory(User::class)->create()->id,
        'created_at' => $faker->dateTime
    ];
});
