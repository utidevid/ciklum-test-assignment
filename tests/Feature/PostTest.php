<?php

namespace Tests\Feature;

use App\Post;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class PostTest extends TestCase
{
    use DatabaseMigrations;

    // todo: Additional methods could be moved to a BaseTestCase class
    protected User $currentUser;
    protected const CURRENT_USER_TOKEN = 'test1';

    /**
     * Create a post that belong to a random new user
     * @return Post
     */
    protected function createAlienPost(): Post
    {
        return factory(Post::class)->create();
    }

    /**
     * Create a post entity for a user
     * @param User|null $user a user to whom a post belongs to. If empty, the post belongs to the current user.
     * @return Post
     */
    protected function createPost(User $user = null): Post
    {
        if (!$user) {
            $user = $this->currentUser;
        }

        return factory(Post::class)->create([
            'user_id' => $user->getAttribute('id')
        ]);
    }

    /**
     * Create a user for tests
     * @param string $token
     * @return User
     */
    protected function createAlienUser(string $token): User
    {
        return factory(User::class)->create([
            'api_token' => hash('sha256', $token)
        ]);
    }

    /**
     * @param string|null $bearerToken
     * @return PostTest
     */
    protected function authorizedRequest(string $bearerToken = null): self
    {
        return $this->withHeader(
            'Authorization',
            'Bearer ' . ($bearerToken ?? static::CURRENT_USER_TOKEN)
        );
    }

    public function setUp(): void
    {
        parent::setUp();

        // Create a current user entity
        $this->currentUser = factory(User::class)->create([
            'name' => 'Abigail',
            'api_token' => hash('sha256', static::CURRENT_USER_TOKEN)
        ]);
    }

    public function testGetPostsEmpty()
    {
        $response = $this->authorizedRequest()->getJson('/api/posts');

        $response->assertOk();
        $response->assertJson([]);
    }

    public function testGetPostsUnauthorized()
    {
        $response = $this->authorizedRequest()->get('/api/posts');

        $response->assertOk();
        $response->assertJson([]);
    }

    public function testGetPostsMany()
    {
        $post = $this->createPost();
        $post2 = $this->createPost();
        $response = $this->authorizedRequest()->get('/api/posts');

        $response->assertOk();
        $response->assertJsonStructure([
            ['id', 'title', 'content', 'description', 'user_id', 'created_at'],
            ['id', 'title', 'content', 'description', 'user_id', 'created_at'],
        ]);
    }

    public function testGetPost()
    {
        $post = $this->createPost();
        $response = $this->authorizedRequest()->get('/api/posts/' . $post->id);
        $response->assertOk();
        $response->assertJson([
            'title' => $post->title,
            'content' => $post->content,
            'description' => $post->description,
            'created_at' => $post->created_at->format('Y-m-d H:i:s')
        ]);
    }

    public function testGetUndefinedPost()
    {
        $response = $this->authorizedRequest()->get('/api/posts/22');
        $response->assertNotFound();
        $response->assertJson([
            'error' => 'Entity not found.'
        ]);
    }

    public function testGetPostUnauthorized()
    {
        $this->createPost();
        $response = $this->get('/api/posts/1');
        $response->assertForbidden();
        $response->assertJson([
            'error' => 'Restricted.'
        ]);
    }

    public function testGetPostOfOtherUser()
    {
        $post = $this->createPost(factory(User::class)->create());
        $response = $this->authorizedRequest()->get('/api/posts/1');
        $response->assertOk();
        $response->assertJson([
            'title' => $post->title,
            'content' => $post->content,
            'description' => $post->description,
            'created_at' => $post->created_at->format('Y-m-d H:i:s')
        ]);
    }

    public function testCreatePost()
    {
        $response = $this->authorizedRequest()
            ->postJson('/api/posts', [
                "title" => "Title 1",
                "content" => "Some sort of interesting content that goes here.",
                "description" => "The article description to be proud of."
            ])
        ;

        $response->assertCreated();
        $response->assertJson([
            "id" => 1,
            "title" => "Title 1",
            "content" => "Some sort of interesting content that goes here.",
            "description" => "The article description to be proud of."
        ]);

        $this->assertDatabaseHas('posts', [
            "id" => 1,
            "title" => "Title 1",
            "content" => "Some sort of interesting content that goes here.",
            "description" => "The article description to be proud of."
        ]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreatePostUnauthorized()
    {
        $response = $this->authorizedRequest('test2')
            ->postJson('/api/posts', [
                "title" => "Title 1",
                "content" => "Some sort of interesting content that goes here.",
                "description" => "The article description to be proud of."
            ])
        ;

        $response->assertForbidden();
        $response->assertJson([
            "error" => 'Restricted.'
        ]);

        $this->assertDatabaseMissing('posts', [
            "id" => 1,
            "title" => "Title 1",
            "content" => "Some sort of interesting content that goes here.",
            "description" => "The article description to be proud of."
        ]);
    }

    public function testUpdatePost()
    {
        $post = $this->createPost();

        $response = $this->authorizedRequest()
            ->patchJson('/api/posts/' . $post->getAttribute('id'), [
                "title" => "Title 1",
                "description" => "The article description to be proud of."
            ])
        ;

        $response->assertOk();
        $response->assertJson([
            "title" => "Title 1",
            'content' => $post->getAttribute('content'),
            "description" => "The article description to be proud of."
        ]);
        $this->assertDatabaseHas('posts', [
            "id" => 1,
            "title" => "Title 1",
            "description" => "The article description to be proud of."
        ]);
    }

    public function testUpdateSomeoneElsesPost()
    {
        $post = $this->createAlienPost();

        $response = $this->authorizedRequest()
            ->patchJson('/api/posts/' . $post->getAttribute('id'), [
                "title" => "Title 1",
                "description" => "The article description to be proud of."
            ])
        ;

        $response->assertUnauthorized();
        $response->assertJson([
            'error' => 'Unauthorized.'
        ]);
        $this->assertDatabaseHas('posts', [
            "title" => $post->getAttribute('title'),
            'content' => $post->getAttribute('content'),
            "description" =>  $post->getAttribute('description')
        ]);
    }

    public function testUpdatePostUnauthorized()
    {
        $post = $this->createPost();

        $response = $this->authorizedRequest('test23')
            ->patchJson('/api/posts/' . $post->getAttribute('id'), [
                "title" => "Title 1",
                "description" => "The article description to be proud of."
            ])
        ;

        $response->assertForbidden();
        $response->assertJson([
            'error' => 'Restricted.'
        ]);

        $this->assertDatabaseHas('posts', [
            "title" => $post->getAttribute('title'),
            'content' => $post->getAttribute('content'),
            "description" =>  $post->getAttribute('description')
        ]);
    }

    public function testDeletePost()
    {

        $post = $this->createPost();
        $post2 = $this->createPost();
        $post3 = $this->createPost();

        $response = $this->authorizedRequest()
            ->delete( '/api/posts/', ['ids' => $post3->id])
        ;

        $response->assertNoContent();

        $this->assertDatabaseMissing('posts', ['id' => $post3->id]);
        $this->assertDatabaseHas('posts', ['id' => $post2->id]);
        $this->assertDatabaseHas('posts', ['id' => $post->id]);
    }

    public function testDeletePosts()
    {
        $post = $this->createPost();
        $post2 = $this->createPost();
        $post3 = $this->createPost();

        $response = $this->authorizedRequest()
            ->delete( '/api/posts/', ['ids' => '1,3'])
        ;

        $response->assertNoContent();

        $this->assertDatabaseMissing('posts', ['id' => 1]);
        $this->assertDatabaseMissing('posts', ['id' => 3]);
        $this->assertDatabaseHas('posts', ['id' => 2]);
    }

    public function testDeleteSomeoneElsesPostsAndMine()
    {
        $post = $this->createPost();
        $post2 = $this->createPost();
        $post3 = $this->createPost();

        $otherUser = $this->createAlienUser('test2');
        $post4 = $this->createPost($otherUser);
        $post5 = $this->createPost($otherUser);

        $response = $this->authorizedRequest('test2')
            ->delete( '/api/posts/', ['ids' => '1,3,5'])
        ;

        $response->assertNoContent();

        $this->assertDatabaseHas('posts', ['id' => 1]);
        $this->assertDatabaseHas('posts', ['id' => 2]);
        $this->assertDatabaseHas('posts', ['id' => 3]);
        $this->assertDatabaseHas('posts', ['id' => 4]);
        $this->assertDatabaseMissing('posts', ['id' => 5]);
    }

    public function testDeleteSingularPost()
    {
        $this->createPost();
        $response = $this->authorizedRequest()
            ->delete( '/api/posts/', ['ids' => '1'])
        ;

        $response->assertNoContent();
        $this->assertDatabaseMissing('posts', ['id' => 1]);
    }

    public function testDeleteSingularPostUnauthorized()
    {
        $this->createPost();
        $response = $this->authorizedRequest('edasdsa')
            ->delete( '/api/posts/', ['ids' => '1'])
        ;

        $response->assertForbidden();
        $this->assertDatabaseHas('posts', ['id' => 1]);
    }

    public function testDeleteSingularPostOfOtherUser()
    {
        $this->createPost(factory(User::class)->create());

        $response = $this->authorizedRequest()
            ->delete( '/api/posts/', ['ids' => '1'])
        ;

        $response->assertUnauthorized();
        $this->assertDatabaseHas('posts', ['id' => 1]);
    }

    public function testDeleteSingularPostWhichDoesNotExist()
    {
        $this->createPost();
        $response = $this->authorizedRequest()
            ->delete( '/api/posts/', ['ids' => '1'])
        ;

        $response->assertNoContent();
        $this->assertDatabaseMissing('posts', ['id' => 1]);

        $response = $this->authorizedRequest()
            ->delete( '/api/posts/', ['ids' => '1'])
        ;
        $response->assertNotFound();
        $this->assertDatabaseMissing('posts', ['id' => 1]);

    }


}
