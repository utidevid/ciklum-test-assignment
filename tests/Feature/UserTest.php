<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTokenGeneration()
    {
        $response = $this->json('POST', '/api/user', ['name' => 'Test 1']);

        $response->assertStatus(201);
        $response->assertJsonStructure(['token']);

        $token = $response->json('token');

        $this->assertDatabaseHas('users', [
            'api_token' => hash('sha256', $token),
            'name' => 'Test 1'
        ]);
    }

    public function testIncorrectTokenGeneration()
    {
        $response = $this->json('POST', '/api/user', []);

        $response->assertStatus(400);
        $response->assertJson(['error' => 'Validation failed.']);

        $this->assertDatabaseMissing('users', [
            'name' => 'Test 1'
        ]);
    }
}
