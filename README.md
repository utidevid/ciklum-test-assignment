
# Test Assignment for Ciklum  
## Basic Laravel App  
The application sums up basic RESTful API endpoint to manage posts.
## Installation
Make sure you have docker installed on your local machine to build and run the project. After that, follow the instructions below:

Clone the project:
```bash
git clone git@bitbucket.org:utidevid/ciklum-test-assignment.git
``` 
In the project root directory, run the following command to build the containers:
```bash
docker-compose up -d --build
```
After that, finish up with laravel-specific commands:
```bash
docker exec -it ciklum_app composer install
docker exec -it ciklum_app php artisan migrate:fresh 
```
## Run tests
Run the following command to execute tests:
```bash
docker exec -it ciklum_app vendor/bin/phpunit
```
## API Documentation
### Protected Endpoints
The `/api/posts` endpoint is protected with an auth authorization middleware and requires a Bearer Token in order to be traversed through.
### Token obtaining
A Bearer Token can be obtained at `POST /api/user`. Consequently, a user entity will be created along with a token returned in the response.
### Endpoints
- **POST**      */api/user* - returns a user-specific token - unprotected
```json
{
    "name": "John"
}
```
- **GET**       */api/posts* - Retrieves all posts published by the user
- **GET**       */api/posts/{id}* - Retrieves a specific post published by the user (can view other users' posts)
- **POST**      */api/posts* - Creates a new post
```json
{
    "title": "one 4",
    "content": "Some sort of interesting content that goes here.",
    "description": "The article description to be proud of."
}
```
- **PATCH**     */api/posts/{id}* - Update a post by ID. Can only update author's posts.
```json
{
    "title": "one 4",
    "content": "Some sort of interesting content that goes here.",
    "description": "The article description to be proud of."
}
```
- **DELETE**    */api/posts/?ids=1,2,3,4* - Delete user's posts. Posts' IDs which do not belong to the current user, will be ignored.
## Docker containers info
```bash
CONTAINER ID        IMAGE                        COMMAND                  CREATED             STATUS              PORTS                           NAMES
aa1a21275443        mariadb:latest               "docker-entrypoint.s…"   2 hours ago         Up 2 hours          127.0.0.1:3307->3306/tcp        ciklum_db_test
0b8bcd660be4        ciklum_test_assignment_app   "supervisord -c /etc…"   3 days ago          Up 2 hours          127.0.0.1:80->80/tcp, 443/tcp   ciklum_app
44c8ead517fb        mariadb:latest               "docker-entrypoint.s…"   3 days ago          Up 2 hours          127.0.0.1:3306->3306/tcp        ciklum_db
```
## Routes info
```bash
+--------+----------+------------------+--------------+---------------------------------------------+------------------------------+
| Domain | Method   | URI              | Name         | Action                                      | Middleware                   |
+--------+----------+------------------+--------------+---------------------------------------------+------------------------------+
|        | GET|HEAD | /                |              | Closure                                     | web                          |
|        | GET|HEAD | api/posts        | posts.getAll | App\Http\Controllers\PostsController@getAll | api,auth:api                 |
|        | POST     | api/posts        | posts.create | App\Http\Controllers\PostsController@create | api,auth:api                 |
|        | DELETE   | api/posts        | posts.delete | App\Http\Controllers\PostsController@delete | api,can:delete,auth:api                 |
|        | GET|HEAD | api/posts/{post} | posts.get    | App\Http\Controllers\PostsController@get    | api,auth:api                 |
|        | PATCH    | api/posts/{post} | posts.update | App\Http\Controllers\PostsController@update | api,can:update,post,auth:api |
|        | POST     | api/user         | user.create  | App\Http\Controllers\UsersController@create | api,guest                    |
+--------+----------+------------------+--------------+---------------------------------------------+------------------------------+
```
## Technical Requirements
Application is designed based on the following criteria:  
#### Backend:
- Laravel 6.x
 - API endpoints:     
	 - Get all posts 
	 - Add new post [Post contains Title, Content, Description(8-255 symbols),  Created_at]   
	 - Remove post (with the possibility to remove multiple posts at once)
	 - Validation (fields length, fields types)     
	 - Basic Auth
	 - Cover with functional tests
 #### Additional requirements:  
- Commits should be made by features, not 1 large commit.  
- The project should have readme file with instructions for setting up.  
- Use as many Laravel concepts as can.  
- Preferred to use PHP 7.4
## Ways to improve
- Tests could be improved. More test coverage could be added. 
	- Get all posts and get a single post tests
	- User specific tests
	- More malicious and security penetration tests
- Basic authorisation could make use of JSON web tokens
- More reusable components. E.g. Token generator algorithm is used in tests and in the service. 
- Custom Validation error could be rendered if a certain field is invalid or missing.
	- e.g. *"title should not be more than 100 chars"* or *"content can only be of string data type"*.
- Pagination for get posts
- Links in the API
