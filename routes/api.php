<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('user', 'UsersController@create')
    ->name('user.create')
;
Route::get('posts/{post}', 'PostsController@get')
    ->name('posts.get')
;
Route::get('posts', 'PostsController@getAll')
    ->name('posts.getAll')
;
Route::post('posts', 'PostsController@create')
    ->name('posts.create')
;
Route::delete('posts', 'PostsController@delete')
    ->name('posts.delete');
;
Route::patch('posts/{post}', 'PostsController@update')
    ->name('posts.update')
    ->middleware('can:update,post');
;
